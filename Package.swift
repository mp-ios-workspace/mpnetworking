// swift-tools-version:5.0
// The swift-tools-version declares the minimum version of Swift required to build this package.

import PackageDescription

let package = Package(
    name: "MPNetworking",
    platforms: [
        .iOS(.v10),
        .tvOS(.v10)
    ],
    products: [
        .library(
            name: "MPNetworking",
            targets: ["MPNetworking"]),
    ],
    dependencies: [
    ],
    targets: [
        .target(
            name: "MPNetworking",
            dependencies: [])
    ]
)
