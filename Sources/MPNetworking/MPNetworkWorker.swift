
import Foundation

public protocol MPNetworkWorkerInterface: AnyObject {
    var isLoggingEnabled: Bool { get set }
    
    func cancelDataTask()
    func loadURLRequest(_ urlRequest: URLRequest?, completion: MPNetworkWorker.LoadCompletion<Data>?)
    func loadURLRequest<T: Codable>(_ urlRequest: URLRequest?, decodeTo codableType: T.Type, completion: MPNetworkWorker.LoadCompletion<T>?)
}

open class MPNetworkWorker: NSObject, MPNetworkWorkerInterface {
    
    // MARK: - Static
    public typealias LoadCompletion<DataType> = (DataType?, HTTPURLResponse?, Error?) -> Void
    
    public struct ResponseStatusCode: Equatable {
        
        // MARK: - Props
        public let intValue: Int
        
        // MARK: - Init
        public init(intValue: Int) {
            self.intValue = intValue
        }
    }
    
    // MARK: - Props
    public let identifer: String
    public let session: URLSession
    public let successfulResponseStatusCodes: [ResponseStatusCode]
    
    open var isLoggingEnabled: Bool = true
    open var dataTask: URLSessionDataTask?
    
    // MARK: - Init
    public init(
        session: URLSession,
        successfulResponseStatusCodes: [ResponseStatusCode],
        isLoggingEnabled: Bool,
        identifer: String = UUID().uuidString
        
    ) {
        self.session = session
        self.successfulResponseStatusCodes = successfulResponseStatusCodes
        self.isLoggingEnabled = isLoggingEnabled
        self.identifer = identifer
        
        super.init()
    }
    
    // MARK: - Methods
    open func cancelDataTask() {
        self.dataTask?.cancel()
    }
    
    open func logging(_ items: Any...) {
        guard self.isLoggingEnabled else { return }
        
        var itemsPrint: [String] = [
            "🌎 [\(NSStringFromClass(Self.self).components(separatedBy: ".").last ?? "")] - "
        ]
        itemsPrint.append(contentsOf: items.map { "\($0)" })
        let items = itemsPrint.joined(separator: "").replacingOccurrences(of: ";", with: "\n")
        print(items)
    }
    
    open func loadURLRequest(_ urlRequest: URLRequest?, completion: LoadCompletion<Data>?) {
        guard let urlRequest = urlRequest else {
            completion?(nil, nil, MPNetworkError.invalidRequest)
            return
        }
        
        let completionHandler: (Data?, URLResponse?, Error?) -> Void = {
            [weak self] data, response, error in
            
            guard let response = response as? HTTPURLResponse, error == nil else {
                let networkError: MPNetworkError = .error(error) ?? .unknown
                self?.logging("🟥 [\(#function)] - error:", networkError)
                completion?(nil, nil, networkError)
                
                return
            }
            
            self?.logging(
                "[\(#function)] -;",
                "request.url: \(urlRequest.url?.absoluteString ?? "nil");",
                "request.headres: \(urlRequest.allHTTPHeaderFields ?? [:]);",
                "request.method: \(urlRequest.httpMethod ?? "nil");",
                "request.body: \(urlRequest.httpBody?.prettyPrinted ?? "nil");",
                "response.statusCode: \(response.statusCode);",
                "response.headers: \(response.allHeaderFields as? [String: Any] ?? [:]);",
                "response.data: \(data?.prettyPrinted ?? "nil")"
            )
            
            let isSuccessful = self?.successfulResponseStatusCodes.contains(.init(intValue: response.statusCode)) == true
            guard isSuccessful else {
                let networkError: MPNetworkError = .responseErrorStatusCode(response.statusCode)
                self?.logging("🟥 [\(#function)] - error:", networkError)
                completion?(nil, nil, networkError)
                
                return
            }
            
            completion?(data, response, nil)
        }
        
        self.dataTask?.cancel()
        self.dataTask = self.session.dataTask(with: urlRequest, completionHandler: completionHandler)
        self.dataTask?.resume()
    }
    
    open func loadURLRequest<T: Codable>(_ urlRequest: URLRequest?, decodeTo codableType: T.Type, completion: LoadCompletion<T>?) {
        self.loadURLRequest(urlRequest) { data, response, error in
            guard let data = data, error == nil else {
                let networkError: MPNetworkError = .error(error) ?? .unknown
                self.logging("🟥 [\(#function)] - error:", networkError)
                completion?(nil, nil, networkError)
                
                return
            }
            
            do {
                let dataDecoded = try data.decodeToCodable(codableType)
                
                completion?(dataDecoded, response, nil)
            } catch {
                self.logging("🟥 [\(#function)] - error:", error)
                completion?(nil, nil, error)
            }
        }
    }
    
}

// MARK: - MPURLSession.StatusCode + Store
public extension MPNetworkWorker.ResponseStatusCode {
    
    static var defaultSuccessful: [Self] {
        [
            .init(intValue: 200),
            .init(intValue: 204)
        ]
    }
    
}

// MARK: - MPURLSession.StatusCode + Array
public extension Array where Element == MPNetworkWorker.ResponseStatusCode {
    
    static var defaultSuccessful: Self {
        Element.defaultSuccessful
    }
    
}
