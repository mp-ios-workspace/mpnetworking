
import Foundation

open class MPNetworkError: NSError {}

public extension MPNetworkError {
    
    static var unknown: MPNetworkError {
        .init(domain: "unknown", code: 0, userInfo: nil)
    }
    
    static var invalidRequest: MPNetworkError {
        .init(domain: "invalidRequest", code: 1, userInfo: nil)
    }
    
    static func responseErrorStatusCode(_ statusCode: Int) -> MPNetworkError {
        .init(domain: "responseErrorStatusCode", code: statusCode, userInfo: nil)
    }
    
    static func error(_ error: Error?) -> MPNetworkError? {
        let nsError = error as NSError?
        
        return .init(domain: nsError?.domain ?? "", code: nsError?.code ?? 0, userInfo: nsError?.userInfo)
    }
    
}
