
import Foundation

public protocol MPResponseMappingProtocol: Codable {
    associatedtype ModelType
    
    func mapToModel() -> ModelType?
}

// MARK: - Array + MPResponseMappingProtocol
public extension Array where Element: MPResponseMappingProtocol {
    
    func mapToModels() -> [Element.ModelType] {
        var result: [Element.ModelType] = []
        
        self.forEach({ response in
            guard let model = response.mapToModel() else { return }
            result += [model]
        })
        
        return result
    }
    
}

// MARK: - Array + MPResponseMappingProtocol + Optional
public extension Array {

    func mapOptionalsToModels<T: MPResponseMappingProtocol>() -> [T.ModelType] where Element == T? {
        var result: [T.ModelType] = []
        
        self.forEach({ response in
            guard let model = response?.mapToModel() else { return }
            result += [model]
        })
        
        return result
    }
}
